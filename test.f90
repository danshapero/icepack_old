program test

    use meshes
    use linalg
    use fem
    use netcdf

    ! filenames
    character(len=32) :: arg,sname,bname,uname,vname,tempname,fluxname, &
        & betaname,solname

    ! triangular mesh
    type(tri_mesh) :: glacier
    integer, allocatable :: mask(:)

    ! linear algebra stuff
    class(sparse_matrix), allocatable :: A,M
    class(iterative_solver), allocatable :: stress_solver,mass_solver
    class(preconditioner), allocatable :: stress_pc,mass_pc

    ! elasticity tensor
    real(kind(1d0)), allocatable :: kappa(:,:,:,:,:)

    ! some vectors
    real(kind(1d0)), allocatable :: u(:),h(:),s(:),b(:),f(:),q(:),z(:), &
        & beta(:),vis(:),temp(:),flux(:),nu(:),g(:,:)

    ! physical constants; all units are in MPa - m - years
    real(kind(1d0)), parameter :: yr=3.15576d7, grav=9.81*yr**2, &
        rho_i=917.d0*1d-6/yr**2, rho_w=1020.d0*1d-6/yr**2, &
        Am10=3.985d-13*yr*1d18, Ap10=1.916d3*yr*1d18, &
        & Qm10=6d4, Qp10=139d3, Ry = 8.314d0, &
        & k_i=2.3d0, k_w=2.3d0*0.99d0+0.58d0*0.01d0

    ! local variables
    integer :: i,j,k,l,n,nn,nbnd,elem(3),rows(3)
    real(kind(1d0)) :: area,BE(3,3),beta_avg,speed,stress

    ! variables for reading/writing netCDF files
    integer :: errorcode,netcdfID,nodesID,fieldID,uID,vID



    !----------------------------------------------------------------------!
    ! Parse command-line arguments                                         !
    !----------------------------------------------------------------------!

    sname = 'kangerd/kangerd.2.s.nc          '
    bname = 'kangerd/kangerd.2.b.nc          '
    uname = 'kangerd/kangerd.2.u.nc          '
    vname = 'kangerd/kangerd.2.v.nc             '
    tempname = 'kangerd/kangerd.2.temp.nc       '
    fluxname = 'kangerd/kangerd.2.flux.nc       '

    do i=1,iargc()
        call getarg(i,arg)
        select case(trim(arg))
            case('--out')
                call getarg(i+1,solname)
            case("--help")
                print *, 'This program computes the bed slip paramter beta '
                print *, 'which yields a velocity closest in mean square   '
                print *, 'to a set of observed velocities (u,v).           '
                print *, 'Arguments:                                       '
                print *, '    --mesh : the mesh to use, e.g. kangerd.1     '
                print *, '    --surface : surface elevation                '
                print *, '    --bed : bed elevation                        '
                print *, '    --u : velocity in the x-direction            '
                print *, '    --v : velocity in the y-direction            '
                print *, '    --temp : surface temperature                 '
                print *, '    --flux : basal heat flux                     '
                print *, '    --beta : initial guess for beta (optional)   '
                print *, '    --out : filename for output                  '
                call exit(0)
        end select
    enddo



    !----------------------------------------------------------------------!
    ! Read in the glacier data and do some processing                      !
    !----------------------------------------------------------------------!

    call read_mesh('kangerd/kangerd.2',glacier)
    nn = glacier%nn

    allocate(kappa(2,2,2,2,nn),u(2*nn),h(nn),s(nn),b(nn),f(2*nn),q(2*nn), &
        & z(2*nn),beta(nn),vis(nn),temp(nn),flux(nn),nu(nn),g(2,glacier%ne))

    call check( nf90_open(sname,nf90_nowrite,netcdfID) )
    call check( nf90_inq_varid(netcdfID,'s',fieldID) )
    call check( nf90_get_var(netcdfID,fieldID,s) )
    call check( nf90_close(netcdfID) )

    errorcode = nf90_open(bname,nf90_nowrite,netcdfID)
    errorcode = nf90_inq_varid(netcdfID,'b',fieldID)
    errorcode = nf90_get_var(netcdfID,fieldID,b)
    errorcode = nf90_close(netcdfID)

    errorcode = nf90_open(uname,nf90_nowrite,netcdfID)
    errorcode = nf90_inq_varid(netcdfID,'u',fieldID)
    errorcode = nf90_get_var(netcdfID,fieldID,u(1:2*glacier%nn-1:2))
    errorcode = nf90_close(netcdfID)

    errorcode = nf90_open(vname,nf90_nowrite,netcdfID)
    errorcode = nf90_inq_varid(netcdfID,'v',fieldID)
    errorcode = nf90_get_var(netcdfID,fieldID,u(2:2*glacier%nn:2))
    errorcode = nf90_close(netcdfID)

    call check( nf90_open(tempname,nf90_nowrite,netcdfID) )
    call check( nf90_inq_varid(netcdfID,'temp',fieldID) )
    call check( nf90_get_var(netcdfID,fieldID,temp) )
    call check( nf90_close(netcdfID) )

    call check( nf90_open(fluxname,nf90_nowrite,netcdfID) )
    call check( nf90_inq_varid(netcdfID,'flux',fieldID) )
    call check( nf90_get_var(netcdfID,fieldID,flux) )
    call check( nf90_close(netcdfID) )

    do i=1,nn
        s(i) = max( s(i),40.0 )
        h(i) = min( s(i)-b(i), s(i)/(1.d0-rho_i/rho_w) )

        vis(i) = (2*Ap10*exp(-Qp10/Ry/273.0))**(1.d0/3)*500.d0
        ! After debugging, this will be
        ! vis(i) = (2*Ap10*exp(-Qp10/Ry/(temp+273.d0)))**(-1.d0/3)*h(i)
        ! or vertically averaged

        nu(i) = vis(i)*2000.d0**(-2.d0/3)
    enddo



    !----------------------------------------------------------------------!
    ! Set up the matrices and linear solvers/preconditioners               !
    !----------------------------------------------------------------------!

    nbnd = 0
    do n=1,nn
        if (abs(glacier%bnd(n))==1) nbnd = nbnd+1
        ! After debugging, this will be
        ! if (glacier%bnd(n)==1) nbnd = nbnd+1
    enddo
    allocate(mask(2*nbnd))
    i = 0
    do n=1,nn
        if (abs(glacier%bnd(n))==1) then
        ! if (glacier%bnd(n)==1) then
            mask(i+1) = 2*n-1
            mask(i+2) =  2*n
            i = i+2
        endif
    enddo

    allocate(bsr_matrix::A)
    allocate(csr_matrix::M)

    call A%init(2*nn,2*nn,4*(nn+2*glacier%nl),params=[2,2])
    call M%init(nn,nn,nn+2*glacier%nl)

    call assemble(A,glacier)
    call assemble(M,glacier)

    call mass_matrix(M,glacier)

    allocate(cg_solver::stress_solver)
    allocate(cg_Solver::mass_solver)
    call stress_solver%init(A%nrow,1.d-8)
    call mass_solver%init(M%nrow,1.d-8)

    allocate(nopc::stress_pc)
    allocate(nopc::mass_pc)
    call stress_pc%init(A,2)
    call mass_pc%init(M,2)

    kappa = 0.d0
    do n=1,nn
        do l=1,2
            do k=1,2
                do j=1,2
                    do i=1,2
                        if (i==k .and. j==l) then
                            kappa(i,j,k,l,n) = kappa(i,j,k,l,n)+nu(n)
                        endif
                        if (i==l .and. j==k) then
                            kappa(i,j,k,l,n) = kappa(i,j,k,l,n)+nu(n)
                        endif
                        if (i==j .and. k==l) then
                            kappa(i,j,k,l,n) = kappa(i,j,k,l,n)+2.d0*nu(n)
                        endif
                    enddo
                enddo
            enddo
        enddo
    enddo

    call system_stiffness_matrix(A,glacier,kappa,2)



    !----------------------------------------------------------------------!
    ! Fill the right-hand side vector                                      !
    !----------------------------------------------------------------------!

    g = gradient(glacier,s)
    f = 0.d0
    f(1:2*nn-1:2) = elements_to_nodes(g(1,:),glacier,M,mass_solver,mass_pc)
    f( 2:2*nn:2 ) = elements_to_nodes(g(2,:),glacier,M,mass_solver,mass_pc)

    do i=1,nn
        f(2*i-1) = -(rho_i*grav)*h(i)*f(2*i-1)
        f( 2*i ) = -(rho_i*grav)*h(i)*f( 2*i )
    enddo

    z = 0.d0
    call M%matvec(f(1:2*nn-1:2),z(1:2*nn-1:2))
    call M%matvec(f( 2:2*nn:2 ),z( 2:2*nn:2 ))
    z(mask) = 0.d0



    !----------------------------------------------------------------------!
    ! Add in bed friction                                                  !
    !----------------------------------------------------------------------!

    do i=1,nn
        speed = dsqrt( u(2*i-1)**2+u(2*i)**2 )
        stress = dsqrt( f(2*i-1)**2+f(2*i)**2 )
        beta(i) = dsqrt(stress/speed)
    enddo

    do n=1,glacier%ne
        elem = glacier%elem(:,n)
        beta_avg = sum( beta(elem)**2 )/3.d0

        BE(1,1:2) = glacier%x(:,elem(2))-glacier%x(:,elem(3))
        BE(2,1:2) = glacier%x(:,elem(1))-glacier%x(:,elem(3))
        area = 0.5*dabs( BE(1,1)*BE(2,2)-BE(1,2)*BE(2,1) )
        BE = beta_avg*area/12.d0
        do i=1,3
            BE(i,i) = BE(i,i)+beta_avg*area/12.d0
            if (b(elem(i))+h(elem(i))<(1.d0-(rho_i/rho_w))*h(elem(i))) then
                BE(i,:) = 0.d0
                BE(:,i) = 0.d0
            endif
        enddo

        rows = 2*elem-1
        call A%add_values(rows,rows,BE)

        rows = 2*elem
        call A%add_values(rows,rows,BE)
    enddo



    !----------------------------------------------------------------------!
    ! Solve the linear system                                              !
    !----------------------------------------------------------------------!

    call stress_solver%solve(A,u,z,stress_pc,mask)
    print *, 'Iterations required: ', stress_solver%iterations



    !----------------------------------------------------------------------!
    ! Output the results to a file                                         !
    !----------------------------------------------------------------------!

    errorcode = nf90_create(trim(solname)//'.nc',nf90_clobber,netcdfID)
    errorcode = nf90_def_dim(netcdfID,'nodes',nn,nodesID)
    errorcode = nf90_def_var(netcdfID,'u',nf90_double,nodesID,uID)
    errorcode = nf90_def_var(netcdfID,'v',nf90_double,nodesID,vID)
    errorcode = nf90_enddef(netcdfID)
    errorcode = nf90_close(netcdfID)
    errorcode = nf90_open(trim(solname)//'.nc',nf90_write,netcdfID)
    errorcode = nf90_put_var(netcdfID,uID,u(1:2*nn-1:2))
    errorcode = nf90_put_var(netcdfID,vID,u( 2:2*nn:2 ))
    errorcode = nf90_close(netcdfID)


contains

subroutine check(stat)
    implicit none
    integer, intent(in) :: stat

    if (stat/=nf90_noerr) then
        print *, trim(nf90_strerror(stat))
    endif
end subroutine check

end program test
