program stress_and_mass_balance

    use shallow_shelf_approximation
    use meshes
    use linalg
    use fem
    use netcdf

    ! filenames for input parameters
    character(len=32) :: meshname,datname,uname,outname,betaname,arg

    ! shallow shelf object
    type(shallow_shelf) :: ssa

    ! some locals
    integer :: i
    real(kind(1d0)), allocatable :: mass_balance(:), flux(:,:), g(:,:)

    ! variables for reading/writing NetCDF files
    integer :: netcdfID, nodesID, uID, vID, mbID, stressID, errorcode


    !----------------------------------------------------------------------!
    ! Parse command line arguments                                         !
    !----------------------------------------------------------------------!
    betaname = 'none                            '
    do i=1,iargc()
        call getarg(i,arg)
        select case(trim(arg))
            case('--mesh')
                call getarg(i+1,meshname)
            case('--data')
                call getarg(i+1,datname)
            case('--u')
                call getarg(i+1,uname)
            case('--out')
                call getarg(i+1,outname)
            case('--help')
        end select
    enddo


    !----------------------------------------------------------------------!
    ! Set up the shallow shelf object                                      !
    !----------------------------------------------------------------------!
    call setup(ssa,meshname, &
        & trim(datname)//'.s.nc',trim(datname)//'.b.nc', &
        & uname, uname, &
        & trim(datname)//'.temp.nc',trim(datname)//'.flux.nc',betaname)

    call driving_stress(ssa)


    !----------------------------------------------------------------------!
    ! Compute the mass balance                                             !
    !----------------------------------------------------------------------!
    allocate(flux(2,ssa%glacier%nn),g(2,ssa%glacier%ne), &
        & mass_balance(ssa%glacier%nn))
    flux(1,:) = h*ssa%u(1:2*ssa%glacier%nn-1:2)
    flux(2,:) = h*ssa%u(2:2*ssa%glacier%nn:2)

    mass_balance = 0.d0

    g = gradient(ssa%glacier,flux(1,:))

    mass_balance = elements_to_nodes(g(1,:),ssa%glacier, &
        & ssa%M,ssa%mass_solver,ssa%mass_pc)

    g = gradient(ssa%glacier,flux(2,:))

    mass_balance = mass_balance &
                & +elements_to_nodes(g(2,:),ssa%glacier, &
        & ssa%M,ssa%mass_solver,ssa%mass_pc)


    !----------------------------------------------------------------------!
    ! Save the driving stress to a file                                    !
    !----------------------------------------------------------------------!
    errorcode = nf90_create('driving_stress.nc',nf90_clobber,netcdfID)
    errorcode = nf90_def_dim(netcdfID,'nodes',ssa%glacier%nn,nodesID)
    errorcode = nf90_def_var(netcdfID,'stressx',nf90_double,nodesID,uID)
    errorcode = nf90_def_var(netcdfID,'stressy',nf90_double,nodesID,vID)
    errorcode = nf90_enddef(netcdfID)
    errorcode = nf90_close(netcdfID)
    errorcode = nf90_open('driving_stress.nc',nf90_write,netcdfID)
    errorcode = nf90_put_var(netcdfID,uID,ssa%f(1:2*ssa%glacier%nn-1:2))
    errorcode = nf90_put_var(netcdfID,vID,ssa%f(2:2*ssa%glacier%nn:2))
    errorcode = nf90_close(netcdfID)


    !----------------------------------------------------------------------!
    ! Save the mass balance to a file                                      !
    !----------------------------------------------------------------------!
    errorcode = nf90_create(trim(outname)//'.nc',nf90_clobber,netcdfID)
    errorcode = nf90_def_dim(netcdfID,'node',ssa%glacier%nn,nodesID)
    errorcode = nf90_def_var(netcdfID,'a',nf90_double,nodesID,mbID)
    errorcode = nf90_enddef(netcdfID)
    errorcode = nf90_close(netcdfID)
    errorcode = nf90_open(trim(outname)//'.nc',nf90_write,netcdfID)
    errorcode = nf90_put_var(netcdfID,mbID,mass_balance)
    errorcode = nf90_close(netcdfID)


end program stress_and_mass_balance
