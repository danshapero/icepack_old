module shallow_shelf_approximation

    use meshes
    use linalg
    use fem
    use netcdf

    implicit none

    type :: shallow_shelf
        ! Glacier geometry
        type(tri_mesh) :: glacier

        ! Field variables: ice velocity, thickness, surface elevation, 
        ! bed elevation, basal slip, viscosity parameter
        real(kind(1d0)), allocatable :: u(:),h(:),s(:),b(:),beta(:),vis(:),&
            & temp(:),flux(:)

        ! Other variables: viscosity coefficient, strain rate, viscosity
        ! tensor, driving stress, front stress
        real(kind(1d0)), allocatable :: nu(:),strain(:,:,:), &
            & kappa(:,:,:,:,:),f(:),fr(:)

        ! Matrices and linear solvers
        class(sparse_matrix), allocatable :: A,M
        class(iterative_solver), allocatable :: stress_solver,mass_solver
        class(preconditioner), allocatable :: stress_pc,mass_pc
        integer, allocatable :: mask(:)
    end type shallow_shelf

    ! All units are in MPa - m - years
    real(kind(1d0)), parameter :: yr=3.15576d7, grav=9.81*yr**2, &
        rho_i=917.d0*1d-6/yr**2, rho_w=1020.d0*1d-6/yr**2, &
        Am10=3.985d-13*yr*1d18, Ap10=1.916d3*yr*1d18, &
        & Qm10=6d4, Qp10=139d3, Ry = 8.314d0, &
        & kappa_i=2.3d0, kappa_w=2.3d0*0.99d0+0.58d0*0.01d0


contains



!--------------------------------------------------------------------------!
subroutine setup(ssa,meshname,sname,bname,uname,vname,tempname,fluxname, & !
    & betaname)                                                            !
!--------------------------------------------------------------------------!
    implicit none
    ! input/output variables
    type(shallow_shelf), intent(inout) :: ssa
    character(len=*), intent(in) :: meshname,sname,bname,uname,vname, &
        & tempname,fluxname
    character(len=*), intent(in), optional :: betaname
    ! local variables
    integer :: i,j,k,nbnd,nn,integration_pts
    real(kind(1d0)) :: dz,zm,zk,tk
    ! variables for reading/writing netCDF files
    integer :: netcdfID,errorcode,fieldID

    !------------------
    ! Read in the mesh
    call read_mesh(meshname,ssa%glacier)

    nn = ssa%glacier%nn
    nbnd = 0
    do i=1,nn
        !if ( ssa%glacier%bnd(i)==1 ) nbnd = nbnd+1
        if ( abs(ssa%glacier%bnd(i))==1 ) nbnd = nbnd+1
    enddo

    !--------------------------------------
    ! Allocate all the fields and matrices
    allocate( ssa%h(nn), ssa%s(nn), ssa%b(nn), ssa%u(2*nn), ssa%beta(nn), &
        & ssa%vis(nn), ssa%temp(nn), ssa%flux(nn), ssa%nu(nn) )
    allocate( ssa%f(2*nn), ssa%fr(2*nn) )
    allocate( ssa%strain(2,2,nn), ssa%mask(2*nbnd), ssa%kappa(2,2,2,2,nn) )

    allocate(bsr_matrix::ssa%A)
    allocate(csr_matrix::ssa%M)

    allocate(cg_solver::ssa%stress_solver)
    allocate(cg_solver::ssa%mass_solver)
    allocate(jacobi_preconditioner::ssa%stress_pc)
    allocate(jacobi_preconditioner::ssa%mass_pc)

    !------------------------------------------------
    ! Associate a bunch of variables for conciseness
    associate( glacier=>ssa%glacier, h=>ssa%h, s=>ssa%s, b=>ssa%b, &
    & u=>ssa%u, beta=>ssa%beta, vis=>ssa%vis, nu=>ssa%nu, f=>ssa%f, &
    & strain=>ssa%strain, kappa=>ssa%kappa, mask=>ssa%mask, A=>ssa%A, &
    & stress_solver=>ssa%stress_solver, stress_pc=>ssa%stress_pc, &
    & M=>ssa%M, mass_solver=>ssa%mass_solver, mass_pc=>ssa%mass_pc, &
    & temp=>ssa%temp, flux=>ssa%flux )

    call A%init(2*nn,2*nn,4*(nn+2*glacier%nl),params=[2,2])
    call M%init(nn,nn,nn+2*glacier%nl)

    call assemble(A,glacier)
    call stress_solver%init(A%nrow,1.d-8)

    call assemble(M,glacier)
    call mass_matrix(M,glacier)
    call mass_solver%init(M%nrow,1.d-8)
    call mass_pc%init(M,0)

    errorcode = nf90_open(sname,nf90_nowrite,netcdfID)
    errorcode = nf90_inq_varid(netcdfID,'s',fieldID)
    errorcode = nf90_get_var(netcdfID,fieldID,s)
    errorcode = nf90_close(netcdfID)

    errorcode = nf90_open(bname,nf90_nowrite,netcdfID)
    errorcode = nf90_inq_varid(netcdfID,'b',fieldID)
    errorcode = nf90_get_var(netcdfID,fieldID,b)
    errorcode = nf90_close(netcdfID)

    errorcode = nf90_open(uname,nf90_nowrite,netcdfID)
    errorcode = nf90_inq_varid(netcdfID,'u',fieldID)
    errorcode = nf90_get_var(netcdfID,fieldID,u(1:2*glacier%nn-1:2))
    errorcode = nf90_close(netcdfID)

    errorcode = nf90_open(vname,nf90_nowrite,netcdfID)
    errorcode = nf90_inq_varid(netcdfID,'v',fieldID)
    errorcode = nf90_get_var(netcdfID,fieldID,u(2:2*glacier%nn:2))
    errorcode = nf90_close(netcdfID)

    errorcode = nf90_open(tempname,nf90_nowrite,netcdfID)
    errorcode = nf90_inq_varid(netcdfID,'temp',fieldID)
    errorcode = nf90_get_var(netcdfID,fieldID,temp)
    errorcode = nf90_close(netcdfID)

    errorcode = nf90_open(fluxname,nf90_nowrite,netcdfID)
    errorcode = nf90_inq_varid(netcdfID,'heat_flux',fieldID)
    errorcode = nf90_put_var(netcdfID,fieldID,flux)
    errorcode = nf90_close(netcdfID)

    if (trim(betaname)/="none") then
        errorcode = nf90_open(betaname,nf90_nowrite,netcdfID)
        errorcode = nf90_inq_varid(netcdfID,'beta',fieldID)
        errorcode = nf90_get_var(netcdfID,fieldID,beta)
        errorcode = nf90_close(netcdfID)
    else
        beta = 5d-3
    endif

    !--------------------------------------
    ! Do some processing on the input data
    j = 0
    integration_pts = 8
    do i=1,glacier%nn
        ! Make the mask to enforce Dirichlet boundary conditions
        !if (glacier%bnd(i)==1) then
        if (abs(glacier%bnd(i))==1) then
            mask(j+1) = 2*i-1
            mask(j+2) = 2*i
            j = j+2
        endif

        ! Put a lower threshold on the surface elevation
        s(i) = max( s(i),b(i)+40.0 )

        ! Compute the ice thickness from the surface/bed elevation
        h(i) = min( s(i)-b(i), s(i)/(1.d0-rho_i/rho_w) )

        ! Bed slip paramter is represented as beta**2 for positivity
        beta(i) = dsqrt(beta(i))

        ! viscosity parameter has Arrhenius law temperature dependence
        zm = s(i)+kappa_i*temp(i)/flux(i)
        dz = h(i)/integration_pts
        do k=0,integration_pts
            zk = s(i)-k*dz
            if ( zk>zm ) then
                tk = temp(i)+flux(i)/kappa_i*(s(i)-zk)
                vis(i) = vis(i) &
                !& +(2*Am10*exp(-Qm10/Ry/(tk+273.0)))**(-1.d0/3)*dz
                & +(Am10*exp(-Qm10/Ry/(tk+273.0)))**(-1.d0/3)*dz
            else
                tk = flux(i)/kappa_w*(s(i)+kappa_i*temp(i)/flux(i)-zk)
                vis(i) = vis(i) &
                !& +(2*Ap10*exp(-Qp10/Ry/(tk+273.0)))**(-1.d0/3)*dz
                & +(Ap10*exp(-Qp10/Ry/(tk+273.0)))**(-1.d0/3)*dz
            endif
        enddo

        ! Compute a very blunt approximation to the viscosity coefficient
        ! assuming a strain rate of ~0.2/yr
        nu(i) = vis(i)*2000.d0**(-2.d0/3.d0)
    enddo

    end associate
    
end subroutine setup



!--------------------------------------------------------------------------!
subroutine driving_stress(ssa)                                             !
!--------------------------------------------------------------------------!
    implicit none
    ! input/output variables
    type(shallow_shelf), intent(inout) :: ssa
    ! local variables
    real(kind(1d0)) :: g(2,ssa%glacier%ne)
    integer :: i

    associate( glacier=>ssa%glacier, h=>ssa%h, s=>ssa%s, b=>ssa%b, &
    & u=>ssa%u, beta=>ssa%beta, vis=>ssa%vis, nu=>ssa%nu, f=>ssa%f, &
    & strain=>ssa%strain, kappa=>ssa%kappa, mask=>ssa%mask, A=>ssa%A, &
    & stress_solver=>ssa%stress_solver, stress_pc=>ssa%stress_pc, &
    & M=>ssa%M, mass_solver=>ssa%mass_solver, mass_pc=>ssa%mass_pc )

    g = gradient( glacier, s )
    f = 0.d0
    f(1:2*glacier%nn-1:2) = elements_to_nodes(g(1,:),glacier, &
        & M,mass_solver,mass_pc)
    f(2:2*glacier%nn:2  ) = elements_to_nodes(g(2,:),glacier, &
        & M,mass_solver,mass_pc)

    do i=1,glacier%nn
        f(2*i-1) = -(rho_i*grav)*h(i)*f(2*i-1)
        f( 2*i ) = -(rho_i*grav)*h(i)*f( 2*i )
    enddo

    end associate

end subroutine driving_stress



!--------------------------------------------------------------------------!
subroutine shelf_front_stress(ssa)                                         !
!--------------------------------------------------------------------------!
    implicit none
    ! input/output variables
    type(shallow_shelf), intent(inout) :: ssa
    ! local variables
    integer :: i,j,k,n,elem(3)
    real(kind(1d0)) :: dx,CF,nrm(2),se(3),he(3)

    associate( glacier=>ssa%glacier, h=>ssa%h, s=>ssa%s, b=>ssa%b, &
    & u=>ssa%u, beta=>ssa%beta, vis=>ssa%vis, nu=>ssa%nu, f=>ssa%f, &
    & strain=>ssa%strain, kappa=>ssa%kappa, mask=>ssa%mask, A=>ssa%A, &
    & stress_solver=>ssa%stress_solver, stress_pc=>ssa%stress_pc, &
    & M=>ssa%M, mass_solver=>ssa%mass_solver, mass_pc=>ssa%mass_pc, &
    & fr=>ssa%fr, bnd=>ssa%glacier%bnd, x=>ssa%glacier%x )

    CF = (rho_i*grav)*(1.d0-rho_i/rho_w)/2

    fr = 0.d0
    do n=1,glacier%ne
        elem = glacier%elem(:,n)
        he = h(elem)
        se = s(elem)
        do i=1,3
            if (glacier%neigh(i,n)==-1) then
                j = mod(i,3)+1
                k = mod(i+1,3)+1
                if(bnd(elem(j))==-1 .and. bnd(elem(k))==-1) then
                    dx = dsqrt( (x(1,elem(k))-x(1,elem(j)))**2 &
                        & +(x(2,elem(k))-x(2,elem(j)))**2 )
                    nrm(1) = ( x(2,elem(k))-x(2,elem(j)) )/dx
                    nrm(2) = ( x(1,elem(j))-x(1,elem(k)) )/dx

                    fr(2*elem(j)-1:2*elem(j)) = fr(2*elem(j)-1:2*elem(j)) &
                        & -CF*dx*( he(j)**2/3+he(k)**2/6)*nrm
                    fr(2*elem(k)-1:2*elem(k)) = fr(2*elem(k)-1:2*elem(k)) &
                        & -CF*dx*( he(j)**2/6+he(k)**2/3)*nrm
                endif
            endif
        enddo
    enddo


    end associate

end subroutine



!--------------------------------------------------------------------------!
subroutine bed_friction(ssa)                                               !
!--------------------------------------------------------------------------!
    implicit none
    ! input/output variables
    type(shallow_shelf), intent(inout) :: ssa
    ! local variables
    integer :: i,j,n,rows(3),elem(3)
    real(kind(1d0)) :: BE(3,3),beta_avg,area

    associate( glacier=>ssa%glacier, h=>ssa%h, s=>ssa%s, b=>ssa%b, &
    & u=>ssa%u, beta=>ssa%beta, vis=>ssa%vis, nu=>ssa%nu, f=>ssa%f, &
    & strain=>ssa%strain, kappa=>ssa%kappa, mask=>ssa%mask, A=>ssa%A, &
    & stress_solver=>ssa%stress_solver, stress_pc=>ssa%stress_pc, &
    & M=>ssa%M, mass_solver=>ssa%mass_solver, mass_pc=>ssa%mass_pc )

    do n=1,glacier%ne
        elem = glacier%elem(:,n)

        beta_avg = sum( beta(elem)**2 )/3.d0

        BE(1,1:2) = glacier%x(:,elem(2))-glacier%x(:,elem(3))
        BE(2,1:2) = glacier%x(:,elem(1))-glacier%x(:,elem(3))
        area = 0.5*dabs( BE(1,1)*BE(2,2)-BE(1,2)*BE(2,1) )
        BE = beta_avg*area/12.d0
        do i=1,3
            BE(i,i) = BE(i,i)+beta_avg*area/12.d0
            if (b(elem(i))+h(elem(i))<(1.d0-(rho_i/rho_w))*h(elem(i))) then
                BE(i,:) = 0.d0
                BE(:,i) = 0.d0
            endif
        enddo

        rows = 2*elem-1
        call A%add_values(rows,rows,BE)

        rows = 2*elem
        call A%add_values(rows,rows,BE)
    enddo

    end associate

end subroutine bed_friction



!--------------------------------------------------------------------------!
subroutine diagnostic(ssa)                                                 !
!--------------------------------------------------------------------------!
    implicit none
    ! input/output variables
    type(shallow_shelf), intent(inout) :: ssa
    ! local variables
    integer :: i,j,k,l,n,iter
    real(kind(1d0)) :: u_0(2*ssa%glacier%nn),g(2,ssa%glacier%ne), &
        & q(2*ssa%glacier%nn),relative_error

    associate( glacier=>ssa%glacier, h=>ssa%h, s=>ssa%s, fr=>ssa%fr, &
    & u=>ssa%u, beta=>ssa%beta, vis=>ssa%vis, nu=>ssa%nu, f=>ssa%f, &
    & strain=>ssa%strain, kappa=>ssa%kappa, mask=>ssa%mask, A=>ssa%A, &
    & stress_solver=>ssa%stress_solver, stress_pc=>ssa%stress_pc, &
    & M=>ssa%M, mass_solver=>ssa%mass_solver, mass_pc=>ssa%mass_pc )

    relative_error = 1.d0

    call M%matvec(f(1:2*glacier%nn-1:2),q(1:2*glacier%nn-1:2))
    call M%matvec(f( 2:2*glacier%nn:2 ),q( 2:2*glacier%nn:2 ))

!    q = q+fr

    do while( relative_error > 1.d-2 )
        u_0 = u

        !-----------------------------------------------------------
        ! Compute the viscosity tensor from the current strain rate
        kappa = 0.d0
        do n=1,glacier%nn
            do l=1,2
                do k=1,2
                    do j=1,2
                        do i=1,2
                            if (i==k .and. j==l) then
                                kappa(i,j,k,l,n) = kappa(i,j,k,l,n)+nu(n)
                            endif
                            if (i==l .and. j==k) then
                                kappa(i,j,k,l,n) = kappa(i,j,k,l,n)+nu(n)
                            endif
                            if (i==j .and. k==l) then
                                kappa(i,j,k,l,n) = kappa(i,j,k,l,n)+2*nu(n)
                            endif
                        enddo
                    enddo
                enddo
            enddo
        enddo

        !-------------------------
        ! Build the linear system
        call system_stiffness_matrix(A,ssa%glacier,kappa,2)
        call bed_friction(ssa)

        !-------------------------
        ! Solve the linear system
        call stress_pc%init(A,2)
        call stress_solver%solve(A,u,q,stress_pc,ssa%mask)

        !-----------------------------
        ! Compute the new strain rate
        g = gradient(glacier,u(1:2*glacier%nn-1:2))
        strain(1,1,:) = elements_to_nodes(g(1,:),glacier, &
            & M,mass_solver,mass_pc)
        strain(1,2,:) = elements_to_nodes(g(2,:),glacier, &
            & M,mass_solver,mass_pc)

        g = gradient(glacier,u(2:2*glacier%nn:2))
        strain(2,1,:) = elements_to_nodes(g(1,:),glacier, &
            & M,mass_solver,mass_pc)
        strain(2,2,:) = elements_to_nodes(g(2,:),glacier, &
            & M,mass_solver,mass_pc)

        !---------------------------
        ! Compute the new viscosity
        do i=1,glacier%nn
            nu(i) = vis(i)*dsqrt(sum(strain(:,:,i)**2)+1.d-12)**(-2.d0/3)
        enddo

        relative_error = dsqrt( sum((u-u_0)**2)/sum(u_0**2) )
    enddo

    end associate

end subroutine diagnostic



!--------------------------------------------------------------------------!
subroutine destroy(ssa)                                                    !
!--------------------------------------------------------------------------!
    implicit none
    type(shallow_shelf), intent(inout) :: ssa

    deallocate(ssa%u,ssa%h,ssa%s,ssa%b,ssa%beta,ssa%vis,ssa%temp,ssa%flux, &
        & ssa%nu,ssa%strain,ssa%kappa,ssa%f,ssa%fr,ssa%mask)
    deallocate(ssa%A,ssa%M)
    deallocate(ssa%stress_solver,ssa%mass_solver)
    deallocate(ssa%stress_pc,ssa%mass_pc)
    
end subroutine destroy





end module shallow_shelf_approximation
