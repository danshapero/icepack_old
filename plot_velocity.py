from scipy.io.netcdf import NetCDFFile as dataset
import numpy as np
import matplotlib.pyplot as plt
import sys

nodes = open(sys.argv[1]+'.node','r')

n = int(nodes.readline().split()[0])
x = np.zeros(n)
y = np.zeros(n)
for i in range(n):
    line = nodes.readline().split()
    x[i] = float(line[1])
    y[i] = float(line[2])

ncfile = dataset(sys.argv[2],"r")
u = ncfile.variables['u'][:]
v = ncfile.variables['v'][:]
ncfile.close()

print (np.min(u),np.max(u))

plt.figure()
plt.gca().set_aspect('equal')
plt.quiver(x,y,u,v)
plt.show()
plt.clf()
