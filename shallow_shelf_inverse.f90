program shallow_shelf_inverse

    use shallow_shelf_approximation
    use fem
    use meshes
    use linalg
    use netcdf

    ! filenames
    character(len=32) :: meshname,datname,beta0name,outname,itersname, &
        & regparamname,arg

    ! shllow shelf object
    type(shallow_shelf) :: ssa

    ! other local variables
    integer :: i,j,k,nbnd,iterations=20
    integer, allocatable :: lmask(:)
    real(kind(1d0)) :: misfit(3),alpha(3),mean_square_u,stress,speed
    real(kind(1d0)), allocatable :: lambda(:),dJ(:),u0(:),beta0(:), &
        & p(:),q(:),r(:),eu(:)

    ! variables for Tikhonov regularization
    class(sparse_matrix), allocatable :: TR
    class(iterative_solver), allocatable :: reg_solver
    class(preconditioner), allocatable :: reg_pc
    real(kind(1d0)) :: reg_param
    integer :: reg_mask(0)

    ! variables for reading/writing to netcdf
    integer :: netcdfID,nodesID,betaID,errorcode,euID,evID,fieldID


    !----------------------------------------------------------------------!
    ! Parse command-line arguments                                         !
    !----------------------------------------------------------------------!
    reg_param = 0.d0
    beta0name = 'none                            '
    do i=1,iargc()
        call getarg(i,arg)
        select case(trim(arg))
            case('--mesh')
                call getarg(i+1,meshname)
            case('--data')
                call getarg(i+1,datname)
            case('--beta')
                call getarg(i+1,beta0name)
            case('--out')
                call getarg(i+1,outname)
            case('--iters')
                call getarg(i+1,itersname)
                read(itersname,*) iterations
            case('--reg')
                call getarg(i+1,regparamname)
                read(regparamname,*) reg_param
            case('--help')
                print *, 'This program computes the bed slip paramter beta '
                print *, 'which yields a velocity closest in mean square   '
                print *, 'to a set of observed velocities (u,v).           '
                print *, 'Command-line arguments:                          '
                print *, '  --mesh <path to mesh>                          '
                print *, '           e.g.: --mesh kangerd/kangerd.1        '
                print *, '  --data <path to data>                          '
                print *, '          <path to data> must be the stem of     '
                print *, '          several filenames, which are:          '
                print *, '              <path>/b.nc : bed topography       '
                print *, '              <path>/s.nc : surface topography   '
                print *, '              <path>/temp.nc : surf. temperature '
                print *, '              <path>/flux.nc : basal heat flux   '
                print *, '              <path>/u.nc : x-velocity           '
                print *, '              <path>/eu.nc : error bars for u    '
                print *, '              <path>/v.nc : y-velocity           '
                print *, '              <path>/ev.nc : error bars for v    '
                print *, '  --beta <path to initial guess for bed slip>    '
                print *, '  --out <output file name>                       '
                print *, '  --iters <number of iterations to perform>      '
                call exit(0)
        end select
    enddo


    !----------------------------------------------------------------------!
    ! Load in the shallow shelf data and set up the computation            !
    !----------------------------------------------------------------------!
    call setup(ssa,meshname, &
        & trim(datname)//'.s.nc',trim(datname)//'.b.nc', &
        & trim(datname)//'.u.nc',trim(datname)//'.v.nc', &
        & trim(datname)//'.temp.nc', trim(datname)//'.flux.nc', &
        & beta0name)

    nbnd = 0
    do i=1,ssa%glacier%nn
        if ( ssa%glacier%bnd(i)==-1 ) nbnd = nbnd+1
    enddo

    allocate( lambda(2*ssa%glacier%nn), dJ(ssa%glacier%nn), &
        & p(ssa%glacier%nn), q(2*ssa%glacier%nn), r(2*ssa%glacier%nn), &
        & u0(2*ssa%glacier%nn), eu(2*ssa%glacier%nn), &
        & beta0(ssa%glacier%nn), lmask(2*nbnd) )

    u0 = ssa%u

    errorcode = nf90_open(trim(datname)//'.eu.nc',nf90_nowrite,netcdfID)
    errorcode = nf90_inq_varid(netcdfID,'eu',fieldID)
    errorcode = nf90_get_var(netcdfID,fieldID,eu(1:2*ssa%glacier%nn-1:2))
    errorcode = nf90_close(netcdfID)
    errorcode = nf90_open(trim(datname)//'.ev.nc',nf90_nowrite,netcdfID)
    errorcode = nf90_inq_varid(netcdfID,'ev',fieldID)
    errorcode = nf90_get_var(netcdfID,fieldID,eu( 2:2*ssa%glacier%nn:2 ))
    errorcode = nf90_close(netcdfID)

    j = 0
    do i=1,ssa%glacier%nn
        if (ssa%glacier%bnd(i)==-1) then
            lmask(j+1) = 2*i-1
            lmask(j+2) = 2*i
            j = j+2
        endif
    enddo

    call driving_stress(ssa)
    call shelf_front_stress(ssa)

    ! Make short names for all the variables
    associate( glacier=>ssa%glacier, h=>ssa%h, s=>ssa%s, b=>ssa%b, &
    & u=>ssa%u, beta=>ssa%beta, vis=>ssa%vis, nu=>ssa%nu, f=>ssa%f, &
    & strain=>ssa%strain, kappa=>ssa%kappa, mask=>ssa%mask, A=>ssa%A, &
    & stress_solver=>ssa%stress_solver, stress_pc=>ssa%stress_pc, &
    & M=>ssa%M, mass_solver=>ssa%mass_solver, mass_pc=>ssa%mass_pc, &
    & nn=>ssa%glacier%nn )

    ! Set up the variables for regularizing the problem
    allocate(csr_matrix::TR)
    allocate(cg_solver::reg_solver)
    allocate(ilu_preconditioner::reg_pc)
    call TR%init(nn,nn,nn+2*glacier%nl)
    call assemble(TR,glacier)
    call stiffness_matrix(TR,glacier,reg_param**2)
    call TR%subset_matrix_add(M)
    call reg_solver%init(nn,1.d-8)
    call reg_pc%init(TR,0)

    ! Compute the mean square velocity
    r = u0/eu
    call M%matvec(r(1:2*nn-1:2),q(1:2*nn-1:2))
    call M%matvec(r( 2:2*nn:2 ),q( 2:2*nn:2 ))
    mean_square_u = dot_product(r,q)

    ! Make an initial guess for beta
    if (trim(beta0name)=="none") then
        do i=1,nn
            stress = dsqrt( f(2*i-1)**2+f(2*i)**2 )
            speed  = dsqrt( u0(2*i-1)**2+u0(2*i)**2 )
            beta(i) = dsqrt( stress/speed )
        enddo
    endif
    beta0 = beta

    ! Initial search direction is constant everywhere; if we can improve
    ! the error by adding a fixed value to the slip coefficient, do so
    ! before making any other improvements
    !dJ = 0.001*beta0
    call M%matvec(0.001*beta0,p)
    call reg_solver%solve(TR,dJ,p,reg_pc,reg_mask)

    errorcode = nf90_create(trim(outname)//'.nc',nf90_clobber,netcdfID)
    errorcode = nf90_def_dim(netcdfID,'nodes',nn,nodesID)
    errorcode = nf90_def_var(netcdfID,'beta',nf90_double,nodesID,betaID)
    errorcode = nf90_enddef(netcdfID)
    errorcode = nf90_close(netcdfID)
    errorcode = nf90_open(trim(outname)//'.nc',nf90_write,netcdfID)


    !----------------------------------------------------------------------!
    ! Invert for beta                                                      !
    !----------------------------------------------------------------------!
    do k=1,iterations
        print *, ' '
        print *, 'Iteration ',k

        call diagnostic(ssa)


        !------------------------------------------------------------------!
        ! Find the best value of beta along the present search direction   !
        !------------------------------------------------------------------!
        r = (u-u0)/eu
        call M%matvec(r(1:2*nn-1:2),q(1:2*nn-1:2))
        call M%matvec(r( 2:2*nn:2 ),q( 2:2*nn:2 ))
        misfit(1) = dot_product(r,q)/mean_square_u

        db = 0.001*(sum(dabs(beta))/nn)/maxval(dabs(dJ))
        alpha(1) = 0.d0
        alpha(2) = db
        alpha(3) = 2.d0*db

        do i=2,3
            beta = beta0-alpha(i)*dJ
            call diagnostic(ssa)
            r = (u-u0)/eu
            call M%matvec(r(1:2*nn-1:2),q(1:2*nn-1:2))
            call M%matvec(r( 2:2*nn:2 ),q( 2:2*nn:2 ))
            misfit(i) = dot_product(r,q)/mean_square_u
        enddo

        !----------------------------------------------
        ! Find interval in which to search for minimum
        i = 1
        print *, 'Finding an interval containing minimum'
        print *, '    Stepsize              error       '
        do while( misfit(3)/misfit(2)<1.0 )! .and. i<40 )
            db = 1.1*db
            alpha(1) = alpha(2)
            alpha(2) = alpha(3)
            alpha(3) = alpha(3)+db
            misfit(1) = misfit(2)
            misfit(2) = misfit(3)
            beta = beta0-alpha(3)*dJ
            call diagnostic(ssa)

            r = (u-u0)/eu
            call M%matvec(r(1:2*nn-1:2),q(1:2*nn-1:2))
            call M%matvec(r( 2:2*nn:2 ),q( 2:2*nn:2 ))
            misfit(3) = dot_product(r,q)/mean_square_u
            print *, alpha(3),misfit(3)
            if (mod(i,8)==0) then
                print *, 'Writing progress to ',trim(outname)//'.nc'
                errorcode = nf90_put_var(netcdfID,betaID,beta**2)
            endif
            i = i+1
        enddo

        !------------------
        ! Bisection search
        print *, 'Minimum contained in ',alpha(1),' ',alpha(3)
        print *, 'Performing bisection search'
        i = 0
        do while( dabs(misfit(3)-misfit(1))>1.0E-4 .and. i<25 )
            ! Compute the error at the midpoint of the interval
            alpha(2) = (alpha(1)+alpha(3))/2.d0
            beta = beta0-alpha(2)*dJ
            call diagnostic(ssa)

            r = (u-u0)/eu
            call M%matvec(r(1:2*nn-1:2),q(1:2*nn-1:2))
            call M%matvec(r( 2:2*nn:2 ),q( 2:2*nn:2 ))
            misfit(2) = dot_product(r,q)/mean_square_u

            print *, alpha(1),alpha(3),'|',misfit(1),misfit(2),misfit(3)

            ! If the error at the midpoint is greater than at the left
            ! endpoint, the minimum is in (left,middle)
            if (misfit(2)>misfit(1)) then
                alpha(3) = alpha(2)
                misfit(3) = misfit(2)

            ! If the error at the midpoint is greater than at the right
            ! endpoint, the minimum is in (middle,right)
            elseif (misfit(2)>misfit(3)) then
                alpha(1) = alpha(2)
                misfit(1) = misfit(2)

            ! Otherwise, the error at the middle point is between that of
            ! the right and left, and in order to evaluate which side to
            ! pick for the next search, we have to compute the derivative
            ! of the error at the midpoint
            else
                beta = beta0-(alpha(2)+( alpha(3)-alpha(1) )/100.d0)*z
                call diagnostic(ssa)
                r = (u-u0)/eu
                call M%matvec(r(1:2*nn-1:2),q(1:2*nn-1:2))
                call M%matvec(r( 2:2*nn:2 ),q( 2:2*nn:2 ))
                d_misfit = dot_product(r,q)/mean_square_u-misfit(2)
                if (d_misfit>0) then
                    alpha(3) = alpha(2)
                    misfit(3) = misfit(2)
                else
                    alpha(1) = alpha(2)
                    misfit(1) = misfit(2)
                endif
            endif

            i = i+1
        enddo

        beta = beta0-alpha(2)*dJ
        beta0 = beta

        print *, 'Writing progress to ',trim(outname)//'.nc'
        errorcode = nf90_put_var(netcdfID,betaID,beta**2)


        !------------------------------------------------------------------!
        ! Compute the search direction for next step                       !
        !------------------------------------------------------------------!
        call diagnostic(ssa)

        ! Solve for the Lagrange multipliers; the matrix for the system
        ! has already been found from the initial solve
        r = (u-u0)/eu
        call M%matvec(r(1:2*nn-1:2),q(1:2*nn-1:2))
        call M%matvec(r( 2:2*nn:2 ),q( 2:2*nn:2 ))

        lambda = 0.d0
        call stress_solver%solve(A,lambda,q,stress_pc,lmask)

        ! Compute the gradient of the objective functional
        !do i=1,nn
        !    p(i) = -2.d0*beta(i)*(u(2*i-1)*lambda(2*i-1)+u(2*i)*lambda(2*i))
        !enddo
        !call M%matvec(p,dJ)
        do i=1,nn
            dJ(i) = -2.0*beta(i)*(u(2*i-1)*lambda(2*i-1)+u(2*i)*lambda(2*i))
        enddo
        call M%matvec(dJ,p)
        call reg_solver%solve(TR,dJ,p,reg_pc,reg_mask)

        print *, 'x-velocities:',minval(u(1:2*nn-1:2)),maxval(u(1:2*nn-1:2))
        print *, 'y-velocities:',minval(u( 2:2*nn:2 )),maxval( u(2:2*nn:2 ))

    enddo

    errorcode = nf90_close(netcdfID)


    end associate


    call destroy(ssa)

end program shallow_shelf_inverse
