program shallow_shelf_diagnostic

    use shallow_shelf_approximation
    use meshes
    use linalg
    use fem
    use netcdf

    ! filenames for input parameters
    character(len=32) :: meshname,datname,betaname,outname,arg

    ! shallow shelf object
    type(shallow_shelf) :: ssa

    ! other local variables
    integer :: i

    ! variables for reading/writing to netcdf
    integer :: netcdfID,nodesID,uID,vID,errorcode


    !----------------------------------------------------------------------!
    ! Parse command line arguments                                         !
    !----------------------------------------------------------------------!
    betaname = 'none                            '
    do i=1,iargc()
        call getarg(i,arg)
        select case(trim(arg))
            case('--mesh')
                call getarg(i+1,meshname)
            case('--data')
                call getarg(i+1,datname)
            case('--beta')
                call getarg(i+1,betaname)
            case('--out')
                call getarg(i+1,outname)
            case('--help')
                print *, 'This program solves the diagnostic equations for '
                print *, 'an ice shelf, using the shallow shelf model.     '
                print *, 'Command-line arguments:                          '
                print *, '  --mesh <path to mesh>                          '
                print *, '          e.g.: --mesh kangerd/kangerd.1         '
                print *, '  --data <path to data>                          '
                print *, '          <path to data> must be the stem of     '
                print *, '          several filenames, which are:          '
                print *, '              <path>/b.nc : bed topography       '
                print *, '              <path>/s.nc : surface topography   '
                print *, '              <path>/temp.nc : surf. temperature '
                print *, '              <path>/flux.nc : basal heat flux   '
                print *, '              <path>/u.nc : x-velocity           '
                print *, '              <path>/v.nc : y-velocity           '
                print *, '  --beta <path to bed slip parameter>            '
                print *, '  --out <output file name>                       '
                call exit(0)
        end select
    enddo


    !----------------------------------------------------------------------!
    ! Set up the shallow shelf object                                      !
    !----------------------------------------------------------------------!
    call setup(ssa,meshname, &
        & trim(datname)//'.s.nc',trim(datname)//'.b.nc', &
        & trim(datname)//'.u.nc',trim(datname)//'.v.nc', &
        & trim(datname)//'.temp.nc',trim(datname)//'.flux.nc',betaname)

    call driving_stress(ssa)
    call shelf_front_stress(ssa)


    !----------------------------------------------------------------------!
    ! Run the diagnostic solver                                            !
    !----------------------------------------------------------------------!
    call diagnostic(ssa)


    !----------------------------------------------------------------------!
    ! Output the results to a file                                         !
    !----------------------------------------------------------------------!
    errorcode = nf90_create(trim(outname)//'.nc',nf90_clobber,netcdfID)
    errorcode = nf90_def_dim(netcdfID,'nodes',ssa%glacier%nn,nodesID)
    errorcode = nf90_def_var(netcdfID,'u',nf90_double,nodesID,uID)
    errorcode = nf90_def_var(netcdfID,'v',nf90_double,nodesID,vID)
    errorcode = nf90_enddef(netcdfID)
    errorcode = nf90_close(netcdfID)
    errorocde = nf90_open(trim(outname)//'.nc',nf90_write,netcdfID)
    errorcode = nf90_put_var(netcdfID,uID,ssa%u(1:2*ssa%glacier%nn-1:2))
    errorcode = nf90_put_var(netcdfID,vID,ssa%u(2:2*ssa%glacier%nn:2))
    errorocde = nf90_close(netcdfID)


    !----------------------------------------------------------------------!
    ! Deallocate all the arrays used                                       !
    !----------------------------------------------------------------------!
    call destroy(ssa)


end program shallow_shelf_diagnostic
