import numpy as np

def read_mesh(filename):
    nodes = open(filename+".node","r")
    n = int(nodes.readline().split()[0])
    x = np.zeros(n)
    y = np.zeros(n)
    bnd = np.zeros(n,dtype=np.int)

    for i in range(n):
        line = nodes.readline().split()
        x[i] = float(line[1])
        y[i] = float(line[2])
        bnd[i] = int(line[3])

    nodes.close()

    elements = open(filename+".ele","r")
    m = int(elements.readline().split()[0])
    ele = np.zeros((m,3),dtype=np.int)

    for i in range(m):
        line = elements.readline().split()
        ele[i][:] = map(int,line[1:])
        for j in range(3):
            ele[i,j] = ele[i,j]-1

    elements.close()

    return(x,y,ele,bnd)
