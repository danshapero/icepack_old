import os
import matplotlib.pyplot as plt
import numpy as np
from scipy.io.netcdf import NetCDFFile as dataset
from read_tri_mesh import *
import math

(x,y,ele,bnd) = read_mesh('kangerd/kangerd.3')
nn = len(x)

ncfile = dataset('driving_stress.nc','r')
sx = ncfile.variables['stressx'][:]
sy = ncfile.variables['stressy'][:]
ncfile.close()
s = np.zeros(nn)
for i in range(nn):
    s[i] = math.sqrt(sx[i]**2+sy[i]**2)

stress = np.zeros(nn)
ratio = np.zeros(nn)
mb = np.zeros(nn)
filenames = ['_600.0_50','_750.0_50','_1000.0_50']
for filename in filenames:
    ncfile = dataset('beta'+filename+'.nc','r')
    beta = ncfile.variables['beta'][:]
    ncfile.close()

    ncfile = dataset('u'+filename+'.nc','r')
    u = ncfile.variables['u'][:]
    v = ncfile.variables['v'][:]
    ncfile.close()

    for i in range(nn):
        stress[i] = beta[i]*math.sqrt(u[i]**2+v[i]**2)
        ratio[i] = min(stress[i]/s[i],1.05)

    plt.figure()
    plt.subplot(1,2,1)
    plt.gca().set_aspect('equal')
    plt.tricontourf(x/1000.0,y/1000.0,ele,stress,36,shading='faceted')
    plt.colorbar()
    plt.xlabel('x (km)')
    plt.ylabel('y (km)')
    plt.title('Bed stress of Kangerd')

    plt.subplot(1,2,2)
    plt.gca().set_aspect('equal')
    plt.tricontourf(x/1000.0,y/1000.0,ele,ratio,np.linspace(0.0,1.08,36),shading='facted')
#    plt.colorbar()
    plt.title('Bed stress / driving stress')

    plt.savefig('stress'+filename+'.png',dpi=480)

    ncfile = dataset('mass_balance'+filename+'.nc','r')
    mb[:] = ncfile.variables['a'][:]
    ncfile.close()
    for i in range(nn):
        mb[i] = min( max(mb[i],-3.0), 3.0 )

    plt.figure()
    plt.gca().set_aspect('equal')
    plt.tricontourf(x/1000.0,y/1000.0,ele,mb,np.linspace(-3.03,3.03,36),shading='faceted')
    plt.colorbar()
    plt.xlabel('x (km)')
    plt.ylabel('y (km)')
    plt.title('Mass balance of Kangerd')
    plt.savefig('mass_balance'+filename+'.png',dpi=480)
